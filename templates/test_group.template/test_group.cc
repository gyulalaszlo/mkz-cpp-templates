#include "test_helpers.h"

<%= generated_header %>

namespace 
{
    struct <%= struct_name %> : public test::Group
    {
        <%= struct_name %>() : test::Group( "<%= struct_name %>") {}

        virtual void describe()
        {
            test_case( <%= struct_name %> , truth );
        }

        
        virtual void before_all()
        {
            // ...
        }

        virtual void after_all()
        {
            // ...
        }

        virtual void before_each()
        {
            memory_globals::init();
        }

        virtual void after_each()
        {
            // detect leaks
            memory_globals::shutdown();
        }

        //////////////////////////////////////////////////////////////////////////

        void truth()
        {
            expect_eq( 1, 1 );
        }
    };
    
    // Creating a static instance of this group registers it for running
    static <%= struct_name %> <%= file_name %>;
}