#include "<%= file_name %>.h"
<%= generated_header %>

#include <new>
#include "foundation/memory.h"

using namespace foundation;
<% nested_namespaces( options[:namespace] ) do |ns| %>
  using namespace <%= ns %>; <% end %>

<% implementation_name = "#{struct_name}Implementation" %>

namespace 
{

  class <%= implementation_name %>
    : public <%= struct_name %>
  {
    friend class <%= struct_name %>;

    ////////////////////////////////////////
    
    <%= implementation_name %>( Allocator& a )
      : allocator( a )
    {
    }

    ~<%= implementation_name %>()
    {
    }

    ////////////////////////////////////////
    // Disallow copy and assign
    <%= implementation_name %>( const <%= implementation_name %>& );
    <%= implementation_name %>& operator=( const <%= implementation_name %>& );
    ////////////////////////////////////////


    Allocator& allocator;

  };

}



<% options[:namespace].each do |ns| %> 
namespace <%= ns %> { <% end %>

  <%= struct_name %>* <%= struct_name %>::make( Allocator& allocator )
  {
    return MAKE_NEW( allocator, <%= implementation_name %>, allocator );
  }

  void <%= struct_name %>::destroy( <%= struct_name %>* manager )
  {
    <%= implementation_name %>* mgr = dynamic_cast<<%= implementation_name %>*>
      ( manager );
    MAKE_DELETE( mgr->allocator, <%= implementation_name %>, mgr );
  }


<% options[:namespace].each do |ns| %> 
} <% end %>
