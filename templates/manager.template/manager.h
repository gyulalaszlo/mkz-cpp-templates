#pragma once
<%= generated_header %>

#include "foundation/memory_types.h"

<% options[:namespace].each do |ns| %> 
namespace <%= ns %> { <% end %>

class <%= struct_name %>
{
  public:
    virtual ~<%= struct_name %>() {}


    // The default implementation
    static <%= struct_name %>* make( foundation::Allocator& a );
    static void destroy( <%= struct_name %>* instance );
};


<% options[:namespace].each do |ns| %> 
} <% end %>
