#include "<%= file_name %>.h"
<%= generated_header %>

#include "foundation/memory.h"

using namespace foundation;
<% nested_namespaces( options[:namespace] ) do |ns| %>
  using namespace <%= ns %>; <% end %>

namespace 
{
  // Implementations
  // ----------------------------------------
}



<% options[:namespace].each do |ns| %> 
namespace <%= ns %> { <% end %>
  // Interfaces
  // ----------------------------------------


<% options[:namespace].each do |ns| %> 
} <% end %>
