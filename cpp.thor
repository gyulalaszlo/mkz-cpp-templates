
class Cpp < Thor
  include Thor::Actions


  class_option :namespace, default: %w(monkeykingz), type: :array,
    banner: 'monkeykingz views component_adapters',
    desc: 'The nesting of C++ namespaces to put the declarations into.'

  class_option :author, default: "Monkeykingz Audio", type: :string,
    desc: 'The author to display'

  class_option :templates, default: [ File.join( File.dirname(__FILE__), 'templates' ) ], type: :array,
    desc: 'A list of directories to search for templates'


  class_option :style, default: true, type: :boolean,
    desc: 'Run the coding style tool on the generated files?'

  class_option :style_command, default: "astyle --options=\"#{File.join(File.dirname(__FILE__), '.astylerc')}\"", type: :string,
    desc: 'The command to issue to format the generated source to a coding standard.'


  class_option :git, default: false, type: :boolean,
    desc: 'Add the generated files to git?'


  desc 'gen TEMPLATE_NAME FILE_NAME OUTPUT_DIR', 'Create C++ file <FILE_NAME> from a <TEMPLATE> in directory <OUTPUT_DIR>'
  def gen template_name, file_name, path
    set_names( file_name )
    all_templates = find_all_templates
    unless all_templates[template_name]
      puts "Cannot find template: #{template_name}"
      return -1
    end
    empty_directory path
    inside path do
      process_template all_templates, template_name, file_name
    end
  end



  method_option :glob, default: "**/*.{h,cc,cpp}", desc: "The file glob to style"
  desc "style PATH", "Apply the coding style tool"
  def style path
    inside path do
      say_status :inside, path
      puts options[:glob]
      Dir[options[:glob]].each do |f|
        say_status :style, f
        apply_style f
      end
    end
  end


  desc 'list', 'Get a list of templates and show their README.md files'
  def list
    all_templates = find_all_templates
    puts "Loading templates from: #{options[:templates].join(", ")}"
    all_templates.each_pair do |name, dir|
      puts ' ', name, '-' * name.size
      puts "  Usage: cpp:gen #{name} <file_name> <directory>"
      readme = get_template_readme(dir)
      next unless readme
      readme.lines.each do |l|
        puts "  #{l}"
      end

    end
  end

  private

  # Get a hash of all the templates by name
  def find_all_templates
    o = {}
    options[:templates].each do |tpl_dir|
      Dir[File.join(tpl_dir, "*.template")].each do |tpl|
        template_name = File.basename tpl, '.template'
        o[ template_name ] = tpl
      end
    end
    o
  end


  def process_template templates, template_name, file_name
    tpl_dir = templates[template_name]
    self.class.source_root( tpl_dir )
    Dir[ File.join( tpl_dir, "*.*" ) ].each do |tpl_file|
      next unless File.basename( tpl_file, File.extname( tpl_file ) ) == template_name
      output_file = "#{file_name}#{File.extname(tpl_file)}"
      template tpl_file, output_file
      apply_style( output_file ) if options[:style]
      add_to_git( output_file ) if options[:git]
    end
  end


  def add_to_git path
    run "git add #{path}"
  end


  def apply_style path
    run( "#{options[:style_command]} #{path}", verbose: false)
  end

  def generated_header
    """
    // (C) #{ Time.now.year } - #{ options[:author] } 
    // See LICENSE for more information.
    """
  end

  def set_names file_name
    @file_name = file_name
    @struct_name = file_name.split(/_+/).map {|s|
      s.gsub(/^[a-z]/) { |m| m.capitalize }
    }.join('')
  end


  def nested_namespaces ns_list, &block
    ns_str = []
    ns_list.each do |ns|
      ns_str << ns
      yield ns_str.join('::')
    end
  end


  def get_template_readme tpl_dir
    readme_path = File.join( tpl_dir, 'README.md' )
    if File.exist? readme_path
      return "\n#{File.read( readme_path )}\n\n"
    end
    nil
  end



  attr_reader :file_name, :struct_name


end
